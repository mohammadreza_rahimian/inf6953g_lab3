#!/bin/bash
tput clear

#Updates fetching and Installation...
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"
apt-get -y update
apt-get -y dist-upgrade

#Installing Prerequisites...
echo -e '\E[37;44m'"\033[1m\nInstalling SQL Node...\033[0m"
apt-get -y install libaio1 libaio-dev htop

#Downloading MySQL Cluster binary files...
cd /var/tmp
wget -N http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz
tar -zxvf mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz -C /usr/local
cd /usr/local/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64

#Extracting the ndb_mgm and ndb_mgmd from the archive into a suitable directory
cp bin/ndb_mgm* /usr/local/bin
cd /usr/local/bin
chmod +x ndb_mg*

#Copying configurations files to the corresponding path...
mkdir /var/lib/mysql-cluster
cp /var/tmp/config.ini /var/lib/mysql-cluster

echo -e '\E[34;43m'"\033[1m\nInstalling has been completed.\033[0m"