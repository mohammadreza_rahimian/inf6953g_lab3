#!/bin/bash
tput clear

#Updates fetching and Installation...
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"
apt-get -y update
apt-get -y dist-upgrade

#MySQL Installation...
echo -e '\E[37;44m'"\033[1m\nInstalling MySQL standalone ...\033[0m"
apt-get -y install libaio1 libaio-dev htop mysql-server

echo -e '\E[34;43m'"\033[1m\nInstalling has been completed.\033[0m"