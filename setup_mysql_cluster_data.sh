#!/bin/bash
tput clear

#Updates fetching and Installation...
echo -e '\E[39;46m'"\033[1mInstalling Updates...\033[0m"
apt-get -y update
apt-get -y dist-upgrade

#Installing Prerequisites...
echo -e '\E[37;44m'"\033[1m\nInstalling SQL Node...\033[0m"
apt-get -y install libaio1 libaio-dev htop

#Creating a new mysql user group, and then add a mysql user to this group
groupadd mysql
useradd -g mysql mysql

#Downloading MySQL Cluster binary files...
cd /var/tmp
wget -N http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz
tar -C /usr/local -zxvf mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64.tar.gz 

#Creating alias to facilitate access....
ln -s /usr/local/mysql-cluster-gpl-7.4.10-linux-glibc2.5-x86_64 /usr/local/mysql
export PATH=$PATH:/usr/local/mysql/bin
echo "export PATH=\$PATH:/usr/local/mysql/bin" >> /etc/bash.bashrc

#Copying configurations files to the corresponding path...
cp /var/tmp/my.cnf /etc

#Running installation script...
cd /usr/local/mysql
./scripts/mysql_install_db --user=mysql --datadir=/usr/local/mysql/data

#Setting the necessary permissions for the MySQL server and data directories
chown -R root .
chown -R mysql data
chgrp -R mysql .

#Setting MySQL to start when the operating system is booted up
cp support-files/mysql.server /etc/init.d/mysql
chmod +x /etc/init.d/mysql
update-rc.d mysql defaults

echo -e '\E[34;43m'"\033[1m\nInstalling has been completed.\033[0m"