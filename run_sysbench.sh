#!/bin/bash
tput clear

echo -e '\E[37;44m'"\033[1m\nRunning SysBench Locally...\033[0m"
DBName=test_standalonedb
USER=root
PWD=''
DBDRV=innodb
HOST=localhost
SIZE=300000
function RunTest {
	##Prepare a test table
	sysbench --test=oltp --oltp-table-size=$SIZE --mysql-db=$DBName --db-driver=$DBDRV --mysql-host=$HOST --mysql-user=$USER --mysql-password=$PWD --mysql-table-engine=$DBDRV prepare

	##Read-Only Test
	sysbench --test=oltp --oltp-table-size=$SIZE --db-driver=$DBDRV --mysql-host=$HOST --mysql-user=$USER --mysql-password=$PWD --mysql-db=$DBName --db-driver=$DBDRV --mysql-host=$HOST --max-time=60 --oltp-read-only=on --max-requests=0 --num-threads=8 --mysql-table-engine=$DBDRV run

	##Mixed Test 8 threads
	sysbench --test=oltp --oltp-table-size=$SIZE --oltp-test-mode=complex --oltp-read-only=off --num-threads=8 --max-time=60 --max-requests=0 --mysql-db=$DBName --db-driver=$DBDRV --mysql-host=$HOST --mysql-user=$USER --mysql-password=$PWD --mysql-table-engine=$DBDRV run

	##Cleanup the test table
	sysbench --test=oltp --mysql-db=$DBName --db-driver=$DBDRV --mysql-host=$HOST --mysql-user=$USER --mysql-password=$PWD --mysql-table-engine=$DBDRV cleanup
 }
#Run sysbench locally
RunTest

#Connect to the remote MySQL and run sysbench
echo -e '\E[37;43m'"\033[1m\nRunning SysBench Remotely...\033[0m"
DBName=test_clusterdb
DBDRV=ndbcluster
HOST=172.31.17.253
RunTest
